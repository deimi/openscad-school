$fn=50;     //smooth it aka use more fragments for circles, global level
// sphere(19, $fn=100);     // smooth it on scope level

difference(){
    cube(30, center=true);
    sphere(19);
}

difference(){
    sphere(16);
    translate([0,0,-30]){
        cube([15,15,30], center=true);  // by making the cube smaller it doesn't interfere with the above cube
        // #cube(30, center=true);      // show the cube despite it was substracted
    }
    // translate([0,0,-30]) cube(30, center=true);
}
